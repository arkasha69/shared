export default function uniqueCategory(Categories, $q) {
  'ngInject';

  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      validate: '=uniqueCategory',
    },
    link,
  };

  function link(scope, elem, attrs, ctrl) {
    let request;

    ctrl.$asyncValidators.uniqueCategory = function uniqueCategoryFn(modelValue, viewValue) {
      const url = modelValue || viewValue;
      let promise;

      if (request) {
        request.$cancelRequest();
        request = null;
      }

      if (scope.validate) {
        request = Categories.get({ id: url, fields: 'url', isExist: true });
        promise = request.$promise.then((response) => response.isExist ? $q.reject('exist') : true);
      } else {
        promise = $q.resolve(true);
      }

      return promise;
    };
  }
}
