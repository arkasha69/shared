export default function uniqueFilterItem() {
  'ngInject';

  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      options: '=uniqueFilterItem',
    },
    link,
  };

  function link(scope, elem, attrs, ctrl) {
    const validate = scope.options.validate;
    const filters = scope.options.filters;

    ctrl.$validators.uniqueFilterItem = function uniqueFilterItemFn(modelValue, viewValue) {
      const url = modelValue || viewValue;

      if (!validate) { return true; }

      for (const filter of filters.list) {
        for (const item of filter.items) {
          if (item.url === url) { return false; }
        }
      }

      return true;
    };
  }
}
