export default function uniqueSubCategory() {
  'ngInject';

  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      options: '=uniqueSubCategory',
    },
    link,
  };

  function link(scope, elem, attrs, ctrl) {
    const validate = scope.options.validate;
    const sublings = scope.options.sublings;

    ctrl.$validators.uniqueSubCategory = function uniqueSubCategoryFn(modelValue, viewValue) {
      const url = modelValue || viewValue;

      if (validate) {
        return !sublings.find((item) => item.url === url);
      }

      return true;
    };
  }
}
