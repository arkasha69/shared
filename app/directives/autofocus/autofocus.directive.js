/**
 * Set focus input on init
 */
export default function autofocusDirective() {
  'ngInject';

  return {
    link: (scope, elem) => elem.focus(),
    restrict: 'A',
  };
}
