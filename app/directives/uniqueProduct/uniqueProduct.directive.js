export default function uniqueProduct(Products, $q) {
  'ngInject';

  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      validate: '=uniqueProduct',
    },
    link,
  };

  function link(scope, elem, attrs, ctrl) {
    let request;

    ctrl.$asyncValidators.uniqueProduct = function uniqueProductFn(modelValue, viewValue) {
      const url = modelValue || viewValue;
      let promise;

      if (request) {
        request.$cancelRequest();
        request = null;
      }

      if (scope.validate) {
        request = Products.get({ id: url, fields: 'url', isExist: true });
        promise = request.$promise.then((response) => response.isExist ? $q.reject('exist') : true);
      } else {
        promise = $q.resolve(true);
      }

      return promise;
    };
  }
}
