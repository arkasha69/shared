export default function uniqueCatalogItem(Catalog, $q) {
  'ngInject';

  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      options: '=uniqueCatalogItem',
    },
    link,
  };

  function link(scope, elem, attrs, ctrl) {
    let request;
    const validate = scope.options.validate;
    const categoryId = scope.options.categoryId;
    const groupId = scope.options.groupId;

    ctrl.$asyncValidators.uniqueCatalogItem = function uniqueCatelogItemFn(modelValue, viewValue) {
      const url = modelValue || viewValue;
      let promise;

      if (request) {
        request.$cancelRequest();
        request = null;
      }

      if (validate) {
        request = Catalog.get({ id: url, fields: 'url', isExist: true, categoryId, groupId });
        promise = request.$promise.then((response) => response.isExist ? $q.reject('exist') : true);
      } else {
        promise = $q.resolve(true);
      }

      return promise;
    };
  }
}
