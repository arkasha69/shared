export default function uniquePage(Pages, $q) {
  'ngInject';

  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      validate: '=uniquePage',
    },
    link,
  };

  function link(scope, elem, attrs, ctrl) {
    let request;

    ctrl.$asyncValidators.uniquePage = function uniquePageFn(modelValue, viewValue) {
      const url = modelValue || viewValue;
      let promise;

      if (request) {
        request.$cancelRequest();
        request = null;
      }

      if (scope.validate) {
        request = Pages.get({ id: url, fields: 'url', isExist: true });
        promise = request.$promise.then((response) => response.isExist ? $q.reject('exist') : true);
      } else {
        promise = $q.resolve(true);
      }

      return promise;
    };
  }
}
