/* global _ */

// TODO Clean up
// Header
import HeaderController from './components/header/header.controller';
import HeaderService from './components/header/header.service';
import addProductsService from './components/addProducts/addProducts.service';
import addProductsController from './components/addProducts/addProducts.controller';

// Constants
// TODO fix implementations with name
import AdminConfigModule from './constants/config/ngConfig';

/* --- Right project structure --- */

/* Configs */
import config from './index.config';
import route from './index.route';
import runBlock from './index.run';

/* Constants */

/* Services */
import BrandsService from './services/brands/brands.service';
import ProductsGroupsService from './services/productsGroups/productsGroups.service';
import ProductsService from './services/products/products.service';
import MenuService from './services/menu/menu.service';
import CategoriesService from './services/categories/categories.service';
import GroupsService from './services/group/group.service';
import CatalogService from './services/catalog/catalog.service';
import TagsService from './services/tags/tags.service';
import PagesService from './services/pages/pages.service';
import HomePageService from './services/home/home.service';
import PricesService from './services/prices/prices.service';
import menuStateService from './services/menuState/menuState.service';
import postsService from './services/posts/posts.service';

/* Filters */

import searchFilter from './filters/search/search.filter';

/* Modules */
import CatalogModule from './catalog/catalog.module';
import blogModule from './blog/blog.module';

/* Directives */
import uniqueProduct from './directives/uniqueProduct/uniqueProduct.directive';
import uniqueCategory from './directives/uniqueCategory/uniqueCategory.directive';
import uniqueCatalogItem from './directives/uniqueCatalogItem/uniqueCatalogItem.directive';
import uniqueSubCategory from './directives/uniqueSubCategory/uniqueSubCategory.directive';
import uniqueFilterItem from './directives/uniqueFilterItem/uniqueFilterItem.directive';
import uniqueTag from './directives/uniqueTag/uniqueTag.directive';
import uniquePage from './directives/uniquePage/uniquePage.directive';
import autofocus from './directives/autofocus/autofocus.directive';

angular.module('admin', [
  'ngAnimate',
  'ngCookies',
  'ngTouch',
  'ngSanitize',
  'ngMessages',
  'ngAria',
  'ui.router',
  'ui.bootstrap',
  'ui.select',
  'toastr',
  'ngResource',
  'ncy-angular-breadcrumb',
  'textAngular',
  'ui.sortable',
  'ngRaven',
  'ngStorage',
  'sticky',
  'ui.indeterminate',
  AdminConfigModule.name,
  CatalogModule,
  blogModule,
])
  .config(config)
  .config(route)
  .run(runBlock)
  /* Controllers */
  .controller('HeaderController', HeaderController)
  .controller('addProductsController', addProductsController)
  /* Services */
  .service('headerService', HeaderService)
  /* Factories */
  .factory('Brands', BrandsService)
  .factory('ProductsGroups', ProductsGroupsService)
  .factory('Products', ProductsService)
  .factory('Menu', MenuService)
  .factory('Categories', CategoriesService)
  .factory('Groups', GroupsService)
  .factory('Catalog', CatalogService)
  .factory('Tags', TagsService)
  .factory('Pages', PagesService)
  .factory('Home', HomePageService)
  .factory('Prices', PricesService)
  .factory('addProductsModal', addProductsService)
  .service('menuState', menuStateService)
  .service('Posts', postsService)
  /* Filters */
  .filter('search', searchFilter)
  /* Directives */
  .directive('uniqueProduct', uniqueProduct)
  .directive('uniqueCategory', uniqueCategory)
  .directive('uniqueCatalogItem', uniqueCatalogItem)
  .directive('uniqueSubCategory', uniqueSubCategory)
  .directive('uniqueFilterItem', uniqueFilterItem)
  .directive('uniqueTag', uniqueTag)
  .directive('uniquePage', uniquePage)
  .directive('autofocus', autofocus)
  /* Other libs */
  .value('_', _)
  .value('lodash', _);
