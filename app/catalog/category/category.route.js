export default function categoryRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.category', {
    url: '/category/:id',
    ncyBreadcrumb: {
      label: '{{ vm.title }}',
      parent: 'home.catalog.menu',
    },
    resolve: {
      category: ($stateParams, Categories, $log) => { // eslint-disable-line
        'ngInject';

        const id = $stateParams.id;

        if (id === 'new') { return new Categories(); }

        return Categories.get({ id }).$promise.then((item) => {
          $log.debug('Category has been loaded', item);
          return item;
        });
      },
    },
    views: {
      '@': {
        controller: 'CategoryController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/category/category.html',
      },
    },
  });
}
