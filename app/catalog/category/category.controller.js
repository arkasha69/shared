const $log = Symbol('$log');
const toastr = Symbol('toastr');
const $state = Symbol('$state');

export default class categoryController {
  constructor(category, _$log_, _toastr_, _$state_) {
    this[$log] = _$log_;
    this[toastr] = _toastr_;
    this[$state] = _$state_;
    this.item = category;

    this.isNew = !this.item._id;

    if (this.isNew) {
      this.title = 'Новая категория';
    } else {
      this.title = this.item.name;
    }
  }

  /**
   * Save category
   */
  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      this[toastr].error('Проверьте корректность введенных данных');
    } else {
      if (this.isNew) {
        this.item.$save(() => {
          this[toastr].info('Категория добавлена');
          this[$state].go('home.catalog.menu');
        }, () => {
          this[toastr].error('Ошибка сервера');
        });
      } else {
        this.item.$update()
          .then(() => {
            this[toastr].info('Категория сохранена');
            this[$state].go('home.catalog.menu');
          }, () => {
            this[toastr].error('Ошибка сервера');
          });
      }
    }
  }
}

categoryController.$inject = ['category', '$log', 'toastr', '$state'];
