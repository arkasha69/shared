import route from './catalog.route';

/* Child modules */
import productsListModule from './productsList/productsList.module';
import productsEditModule from './productsEdit/productsEdit.module';
import brandsModule from './brands/brands.module';
import productsGroupsModule from './productsGroups/productsGroups.module';
import menuModule from './menu/menu.module';
import categoryModule from './category/category.module';
import groupModule from './group/group.module';
import catalogModule from './catalog/catalog.module';
import tagsModule from './tags/tags.module';
import tagsEditModule from './tagsEdit/tagsEdit.module';
import pagesModule from './pages/pages.module';
import pagesEditModule from './pagesEdit/pagesEdit.module';
import homePageModule from './home/home.module';
import pricesModule from './prices/prices.module';

angular
  .module('admin.catalog', [
    'ui.router',
    'ncy-angular-breadcrumb',
    productsListModule,
    brandsModule,
    productsGroupsModule,
    productsEditModule,
    menuModule,
    categoryModule,
    groupModule,
    catalogModule,
    tagsModule,
    tagsEditModule,
    pagesModule,
    pagesEditModule,
    homePageModule,
    pricesModule,
  ])
  .config(route);

export default angular.module('admin.catalog').name;
