import route from './catalog.route';
import Controller from './catalog.controller';

// Child states
import catalogCategoryModule from './category/category.module';
import catalogFiltersModule from './filters/filters.module';

export default angular
  .module('admin.catalog.catalog', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'toastr',
    catalogCategoryModule,
    catalogFiltersModule,
  ])
  .config(route)
  .controller('CatalogController', Controller)
  .name;
