const $log = Symbol('$log');
const toastr = Symbol('toastr');
const $state = Symbol('$state');
const $breadcrumb = Symbol('$breadcrumb');
const _ = Symbol('lodash');
const addProductsModal = Symbol('addProductsModal');
const selectedProducts = Symbol('selectedProducts');
const updateActiveProducts = Symbol('updateActiveProducts');
const updateCategoriesAndFilters = Symbol('updateCategoriesAndFilters');
const $window = Symbol('$window');
const $timeout = Symbol('$timeout');

const updateIndexes = Symbol();

export default class catalogController {
  constructor(category, categoryId, groupId, _$log_, _toastr_, _$state_, _lodash_, _$breadcrumb_
    , _addProductsModal_, _$window_, _$timeout_) {
    this[$log] = _$log_;
    this[toastr] = _toastr_;
    this[$state] = _$state_;
    this[$window] = _$window_;
    this[$timeout] = _$timeout_;
    this[$breadcrumb] = _$breadcrumb_;
    this[_] = _lodash_;
    this[addProductsModal] = _addProductsModal_;
    this[selectedProducts] = new Set();

    this.item = this.category = category;
    this.item.parents = { categoryId, groupId };

    this.isNew = !this.item._id;

    // Set first product as active
    if (this.item.products && this.item.products.length) {
      this.setActiveProduct(this.item.products[0]);
    }

    if (this.isNew) {
      this.title = 'Новая категория';
      this.item.meta = {};
      this.item.descriptions = [];
      this.item.products = [];
      this.item.callback = {
        title: 'Затрудняетесь с выбором?',
        button: 'Получить консультацию',
        visible: true,
      };
      this.item.categories = {
        title: 'Я хочу',
        list: [],
      };
      this.item.filters = {
        title: 'Подобрать под себя',
        list: [],
      };
    } else {
      this.title = this.item.name;
      if (!angular.isArray(this.item.descriptions)) { this.item.descriptions = []; }
      if (!angular.isArray(this.item.products)) { this.item.products = []; }
    }

    // Sortable options
    this.sortableOptions = {
      stop: this[updateIndexes].bind(this),
    };
  }

  /**
   * Adding one extra description item
   */
  addDescription() {
    this.item.descriptions.push({});
  }

  /**
   * Remove extra description item
   */
  removeDescription(item) {
    this[_].remove(this.item.descriptions, item);
  }

  /**
   * Adding product to the category
   */
  addProducts() {
    this[addProductsModal].open(this.item.products)
      .then((newProducts) => {
        newProducts.forEach((newProduct) => {
          if (!this.item.products.find((item) => item._id === newProduct._id)) {
            newProduct.order = this.item.products.length;
            newProduct.categories = [];
            newProduct.filters = [];
            this.item.products.push(newProduct);
          }
          this.setActiveProduct(newProduct, true);
        });

        // this.setActiveProduct(this.item.products[this.item.products.length - 1]);
      });
  }

  /**
   * Remove product from category
   */
  removeProduct(product) {
    this[_].remove(this.item.products, product);

    this[selectedProducts].delete(product);
    if (this[selectedProducts].size === 0 && this.item.products.length) {
      this.setActiveProduct(this.item.products[0]);
    }

    this[updateCategoriesAndFilters]();
    this[updateIndexes]();
  }

  /**
   * Update categories and filters checkboxes for selected products
   * @private
   */
  [updateCategoriesAndFilters]() {
    // Check categories
    this.item.categories.list.forEach((category) => {
      let indeterminate = false;
      let checked;

      for (const productItem of this[selectedProducts]) {
        const isChecked = productItem.categories.indexOf(category._id) !== -1;

        if (angular.isDefined(checked) && checked !== isChecked) {
          [indeterminate, checked] = [true, false];
          break;
        }

        checked = isChecked;
      }

      category.indeterminate = indeterminate;
      category.checked = checked;
    });

    // Check filters
    this.item.filters.list.forEach((filter) => {
      filter.items.forEach((item) => {
        let indeterminate = false;
        let checked;

        for (const productItem of this[selectedProducts]) {
          const isChecked = productItem.filters.indexOf(item._id) !== -1;

          if (angular.isDefined(checked) && checked !== isChecked) {
            [indeterminate, checked] = [true, false];
            break;
          }
          checked = isChecked;
        }

        item.indeterminate = indeterminate;
        item.checked = checked;
      });
    });
  }

  fixSticky() {
    this[$timeout](() => angular.element(this[$window]).triggerHandler('resize'));
  }

  /**
   * Set active products
   * @param {Product}        product Product object
   * @param {Boolean|Object} $event  Append item if true of $event.metaKey
   */
  setActiveProduct(product, $event = false) {
    const multiSelect = (typeof $event === 'boolean' ? $event : $event.metaKey);

    if (this[selectedProducts].size === 1 && this[selectedProducts].has(product)) { return; }

    if (multiSelect) {
      if (this[selectedProducts].has(product)) {
        this[selectedProducts].delete(product);
      } else {
        this[selectedProducts].add(product);
      }
    } else {
      this[selectedProducts].clear();
      this[selectedProducts].add(product);
    }

    this[updateCategoriesAndFilters]();
  }

  /**
   * Update active products after save
   * @private
   */
  [updateActiveProducts]() {
    const newProducts = [];

    this[selectedProducts].forEach((oldProduct) => {
      newProducts.push(this.item.products.find((product) => product._id === oldProduct._id));
    });

    this[selectedProducts].clear();
    newProducts.forEach((product) => this.setActiveProduct(product, true));
  }

  /**
   * Is product active/selected
   */
  isActiveProduct(product) {
    return this[selectedProducts].has(product);
  }

  /**
   * Save category
   */
  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      this[toastr].error('Проверьте корректность введенных данных');
    } else {
      if (this.isNew) {
        this.item.$save((category) => {
          this.item = this.category = category;
          this.isNew = false;
          this.title = this.item.name;
          this[updateActiveProducts]();
          this[$breadcrumb].getLastStep().ncyBreadcrumbLabel = this.item.name;
          this[toastr].info('Категория добавлена');
        }, () => {
          this[toastr].error('Ошибка сервера');
        });
      } else {
        this.item.$update()
          .then(() => {
            this.title = this.item.name;
            this[updateActiveProducts]();
            this[toastr].info('Категория сохранена');
          }, () => {
            this[toastr].error('Ошибка сервера');
          });
      }
    }
  }

  /**
   * Clean filter input
   */
  clearFilters() {
    this.productFilterQuery = '';
  }

  /**
   * Redirect to the new category page for new child category
   */
  addCategory() {
    this[$state].go('.category', { childCategoryId: 'new' });
  }

  /**
   * Redirect to the new filter page for new filter
   */
  addFilter() {
    this[$state].go('.filters', { filterId: 'new' });
  }

  /**
   * Toggle category for selected products
   */
  toggleCategory(category) {
    if (category.checked) {
      this[selectedProducts].forEach((product) => {
        if (product.categories.indexOf(category._id) === -1) {
          product.categories.push(category._id);
        }
      });
    } else {
      this[selectedProducts].forEach((product) => this[_].pull(product.categories, category._id));
    }
  }

  /**
   * Toggle filter for selected products
   * @param  {Filter} filterItem Filter for update
   */
  toggleFilter(filterItem) {
    if (filterItem.checked) {
      this[selectedProducts].forEach((product) => {
        if (product.filters.indexOf(filterItem._id) === -1) {
          product.filters.push(filterItem._id);
        }
      });
    } else {
      this[selectedProducts].forEach((product) => this[_].pull(product.filters, filterItem._id));
    }
  }

  moveUp(list, index) {
    if (index === 0) { return; }

    const item = list[index];
    list[index] = list[index - 1];
    list[index - 1] = item;
  }

  moveDown(list, index) {
    if (index === list.length - 1) { return; }

    const item = list[index];
    list[index] = list[index + 1];
    list[index + 1] = item;
  }

  /**
   * Updating products order fields
   * @private
   */
  [updateIndexes]() {
    this.item.products.forEach((item, index) => item.order = index);
  }
}

catalogController.$inject = [
  'category', 'categoryId', 'groupId', '$log', 'toastr', '$state', '_', '$breadcrumb',
  'addProductsModal', '$window', '$timeout',
];
