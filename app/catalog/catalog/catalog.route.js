export default function categoryRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.catalog', {
    url: '/category/:categoryId/group/:groupId/category/:id',
    ncyBreadcrumb: {
      label: '{{ vm.title }}',
      parent: 'home.catalog.menu',
    },
    resolve: {
      category: ($stateParams, Catalog, $log) => { // eslint-disable-line
        'ngInject';

        const id = $stateParams.id;

        if (id === 'new') { return new Catalog(); }

        return Catalog.get({ id }).$promise.then((item) => {
          $log.debug('Category has been loaded', item);
          return item;
        });
      },
      categoryId: ($stateParams) => $stateParams.categoryId,
      groupId: ($stateParams) => $stateParams.groupId,
    },
    views: {
      '@': {
        controller: 'CatalogController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/catalog/catalog.html',
      },
    },
  });
}
