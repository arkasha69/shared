const $state = Symbol('$state');
const toastr = Symbol('toastr');
const _ = Symbol('lodash');

export default class catalogCategoryController {
  constructor(category, categoryId, _$state_, _toastr_, _lodash_) {
    this[$state] = _$state_;
    this[toastr] = _toastr_;
    this[_] = _lodash_;
    this.category = category;


    this.categoryTitle = category.categories.title;

    this.isNew = categoryId === 'new';

    this.title = category.name;

    if (this.isNew) {
      this.item = {};
      this.subTitle = 'Новая категория';
      this.item.descriptions = [];
      this.item.callback = {
        title: 'Затрудняетесь с выбором?',
        button: 'Получить консультацию',
        visible: true,
      };
    } else {
      this.item = category.categories.list.find((item) => item._id === categoryId);
      this.subTitle = this.item.name;
      if (!angular.isArray(this.item.descriptions)) { this.item.descriptions = []; }
    }
  }

  /**
   * Adding new test block to the category
   */
  addDescription() {
    this.item.descriptions.push({});
  }

  /**
   * Remove extra description item
   */
  removeDescription(item) {
    this[_].remove(this.item.descriptions, item);
  }

  /**
   * Saving changes
   */
  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      this[toastr].error('Проверьте корректность введенных данных');
    } else {
      if (this.isNew) {
        this.category.categories.list.push(this.item);
      }

      this.category.$update(() => {
        if (this.isNew) {
          this[toastr].info('Категория добавлена');
        } else {
          this[toastr].info('Категория сохранена');
        }
        this[$state].go('^');
      }, () => {
        this[toastr].error('Ошибка сервера');
      });
    }
  }
}

catalogCategoryController.$inject = ['category', 'categoryId', '$state', 'toastr', 'lodash'];
