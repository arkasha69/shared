import route from './category.route';
import controller from './category.controller';

export default angular
  .module('admin.catalog.catalog.category', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'toastr',
  ])
  .config(route)
  .controller('CatalogCategoryController', controller)
  .name;
