export default function catalogCategoryRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.catalog.category', {
    url: '/category/:childCategoryId',
    ncyBreadcrumb: {
      label: '{{ vm.subTitle }}',
    },
    resolve: {
      categoryId: ($stateParams) => $stateParams.childCategoryId,
    },
    views: {
      '@': {
        controller: 'CatalogCategoryController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/catalog/category/category.html',
      },
    },
  });
}
