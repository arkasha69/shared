const $state = Symbol('$state');
const toastr = Symbol('toastr');
const _ = Symbol('lodash');

export default class CatalogFiltersController {
  constructor(category, filterId, _$state_, _toastr_, _lodash_) {
    this[$state] = _$state_;
    this[toastr] = _toastr_;
    this[_] = _lodash_;
    this.category = category;
    this.filters = category.filters;

    this.isNew = filterId === 'new';

    this.title = category.name;

    if (this.isNew) {
      this.item = {};
      this.item.items = [{ isNew: true }];
      this.item.order = category.filters.list.length;
      this.subTitle = 'Новый фильтр';
    } else {
      this.item = category.filters.list.find((filter) => filter._id === filterId);
      if (!angular.isArray(this.item.items)) { this.item.items = [{ isNew: true }]; }
      this.subTitle = this.item.title;
    }
  }

  /**
   * Adding new filter item
   */
  addItem() {
    this.item.items.push({ isNew: true });
  }

  /**
   * Removing filter item
   */
  removeItem(itemToRemove) {
    this[_].remove(this.item.items, (item) => item === itemToRemove && item.isNew);
  }

  /**
   * Saving changes
   */
  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      this[toastr].error('Проверьте корректность введенных данных');
    } else if (this.item.items.length === 0) {
      this[toastr].error('Необходимо добавить фильтров');
    } else {
      if (this.isNew) {
        this.category.filters.list.push(this.item);
      }

      this.category.$update(() => {
        if (this.isNew) {
          this[toastr].info('Фильтры добавлены');
        } else {
          this[toastr].info('Фильтры сохранены');
        }
        this[$state].go('^');
      }, () => {
        this[toastr].error('Ошибка сервера');
      });
    }
  }
}

CatalogFiltersController.$inject = ['category', 'filterId', '$state', 'toastr', 'lodash'];
