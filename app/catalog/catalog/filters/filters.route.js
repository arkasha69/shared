export default function CatalogFiltersController($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.catalog.filters', {
    url: '/filters/:filterId',
    ncyBreadcrumb: {
      label: '{{ vm.subTitle }}',
    },
    resolve: {
      filterId: ($stateParams) => $stateParams.filterId,
    },
    views: {
      '@': {
        controller: 'CatalogFiltersController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/catalog/filters/filters.html',
      },
    },
  });
}
