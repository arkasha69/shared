import route from './filters.route';
import Controller from './filters.controller';

export default angular
  .module('admin.catalog.catalog.filters', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'toastr',
  ])
  .config(route)
  .controller('CatalogFiltersController', Controller)
  .name;
