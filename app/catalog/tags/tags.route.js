export default function tagsRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.tags', {
    url: '/tags',
    ncyBreadcrumb: { label: 'Тэги' },
    resolve: {
      tags: (Tags) => Tags.query({ fields: '_id,url,name' }).$promise,
    },
    views: {
      '@': {
        controller: 'TagsController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/tags/tags.html',
      },
    },
  });
}
