import route from './tags.route';
import Controller from './tags.controller';

export default angular
  .module('admin.catalog.tags', [
    'ui.router',
    'ncy-angular-breadcrumb',
  ])
  .config(route)
  .controller('TagsController', Controller)
  .name;
