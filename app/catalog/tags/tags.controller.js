class TagsController {
  constructor(tags, searchFilter, $scope) {
    'ngInject';

    this.query = '';

    this.list = this.filtredList = tags;
    this.totalCount = this.count = tags.length;

    // TODO move watch to the setter
    $scope.$watch(() => this.query, (newValue, oldValue) => {
      if (newValue === oldValue) { return; }

      if (newValue !== '') {
        this.filtredList = searchFilter(this.list, newValue);
      } else {
        this.filtredList = this.list;
      }

      this.count = this.filtredList.length;
    });
  }

  clearFilters() {
    this.query = '';
  }
}

export default TagsController;
