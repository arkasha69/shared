export default function groupRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.group', {
    url: '/group/:id',
    ncyBreadcrumb: {
      label: '{{ vm.title }}',
    },
    resolve: {
      category: ($stateParams, $q, $log) => { // eslint-disable-line
        'ngInject';

        const id = $stateParams.id;

        return $q.resolve({ id });

        // if (id === 'new') { return new Products(); }

        // return Products.get({ id }).$promise;
      },
    },
    views: {
      '@': {
        controller: 'GroupController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/group/group.html',
      },
    },
  });
}
