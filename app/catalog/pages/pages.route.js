export default function tagsRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.pages', {
    url: '/pages',
    ncyBreadcrumb: { label: 'Страницы' },
    resolve: {
      pages: (Pages, $log) =>
        Pages.query({ fields: '_id,url,h1' }).$promise
          .then((pages) => {
            $log.debug('Pages has been loaded', pages);
            return pages;
          }),
    },
    views: {
      '@': {
        controller: 'PagesController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/pages/pages.html',
      },
    },
  });
}
