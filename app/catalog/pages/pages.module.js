import route from './pages.route';
import Controller from './pages.controller';

export default angular
  .module('admin.catalog.pages', [
    'ui.router',
    'ncy-angular-breadcrumb',
  ])
  .config(route)
  .controller('PagesController', Controller)
  .name;
