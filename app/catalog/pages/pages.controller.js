export default class PagesController {
  constructor(pages, searchFilter, $scope) {
    'ngInject';

    this.query = '';

    this.list = this.filtredList = pages;
    this.totalCount = this.count = pages.length;

    // TODO move watch to the setter
    $scope.$watch(() => this.query, (newValue, oldValue) => {
      if (newValue === oldValue) { return; }

      if (newValue !== '') {
        this.filtredList = searchFilter(this.list, newValue);
      } else {
        this.filtredList = this.list;
      }

      this.count = this.filtredList.length;
    });
  }

  clearFilters() {
    this.query = '';
  }
}
