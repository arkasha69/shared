/**
 * New and edit products groups controller
 */
const _toastr = new WeakMap();
const _$state = new WeakMap();

class ProductsGroupsController {
  constructor(group, toastr, $state) {
    'ngInject';

    _toastr.set(this, toastr);
    _$state.set(this, $state);

    /** Group object */
    this.item = group;

    /** Is it new brand */
    this.isNew = !group._id;

    /** Page title */
    this.title = (group._id ? group.name : 'Новая группа товаров');
  }

  /**
   * Save group
   */
  save() {
    const toastr = _toastr.get(this);
    const $state = _$state.get(this);

    this.form.$setSubmitted();

    if (this.form.$invalid) {
      toastr.error('Проверьте корректность введенных данных', 'Группы товаров', { timeOut: 3000 });
    } else {
      if (this.isNew) {
        this.item.$save(() => {
          toastr.info('Добавлено', 'Группы товаров');
          $state.go('^');
        });
      } else {
        this.item.$update()
          .then(() => {
            this.title = this.item.name;

            toastr.info('Сохранено', 'Группы товаров');
          });
      }
    }
  }
}

export default ProductsGroupsController;
