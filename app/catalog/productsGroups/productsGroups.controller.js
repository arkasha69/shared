class ProductsGroupsController {
  constructor($scope, ProductsGroups) {
    'ngInject';

    /** Search query string */
    this.query = '';

    ProductsGroups.query((result) => {
      this.filtredList = this.list = result;
      this.count = this.filtredList.length;
      this.totalCount = this.list.length;
    });

    /** Watch query changes */
    $scope.$watch(() => this.query, (newValue, oldValue) => {
      if (newValue === oldValue) { return; }

      const query = newValue.toLowerCase();

      if (newValue !== '') {
        this.filtredList = this.list.filter((item) =>
          item.name.toLowerCase().indexOf(query) !== -1
        );
      } else {
        this.filtredList = this.list;
      }
      this.count = this.filtredList.length;
    });
  }

  /**
   * Flush all search filters
   */
  clearFilters() {
    this.query = '';
  }
}

export default ProductsGroupsController;
