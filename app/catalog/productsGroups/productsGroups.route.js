function productsGroupsRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.productsGroups', {
    url: '/products-groups',
    ncyBreadcrumb: { label: 'Группа товаров' },
    views: {
      '@': {
        controller: 'ProductsGroupsListController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/productsGroups/productsGroups.html',
      },
    },
  });

  // TODO add link to the parent
  $stateProvider.state('home.catalog.productsGroups.edit', {
    url: '/:id',
    ncyBreadcrumb: { label: '{{ vm.title }}' },
    resolve: {
      group: (ProductsGroups, $stateParams) => {
        if ($stateParams.id === 'new') { return new ProductsGroups(); }

        return ProductsGroups.get({ id: $stateParams.id }).$promise;
      },
    },
    views: {
      '@': {
        controller: 'ProductsGroupsEditController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/productsGroups/productsGroupsEdit.html',
      },
    },
  });
}

export default productsGroupsRoute;
