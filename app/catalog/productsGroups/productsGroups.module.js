import route from './productsGroups.route';
import ListController from './productsGroups.controller';
import EditController from './productsGroupsEdit.controller';

angular
  .module('admin.catalog.productsGroups', [
    'ui.router',
    'ncy-angular-breadcrumb',
  ])
  .config(route)
  .controller('ProductsGroupsListController', ListController)
  .controller('ProductsGroupsEditController', EditController);

export default angular.module('admin.catalog.productsGroups').name;
