function productsRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.products', {
    url: '/products',
    ncyBreadcrumb: { label: 'Товары' },
    views: {
      '@': {
        controller: 'ProductsListController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/productsList/productsList.html',
      },
    },
  });
}

export default productsRoute;
