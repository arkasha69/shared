import route from './productsList.route';
import Controller from './productsList.controller';

angular
  .module('admin.catalog.products', [
    'ui.router',
    'ncy-angular-breadcrumb',
  ])
  .config(route)
  .controller('ProductsListController', Controller);

export default angular.module('admin.catalog.products').name;
