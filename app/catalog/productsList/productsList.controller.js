const query = Symbol('query');
const Products = Symbol('Products');
const productsRequest = Symbol('productsRequest');

export default class ProductsController {
  constructor(_Products_) {
    this[query] = '';
    this[Products] = _Products_;

    this.list = null;
    this.count = null;
  }

  /** Search query getter */
  get query() {
    return this[query];
  }

  /** Search query setter */
  set query(value) {
    this[query] = value;

    if (value.length < 4) return;

    const conditions = { q: value, fields: '_id,url,packs,brand,name' };

    this[productsRequest] && this[productsRequest].$cancelRequest(); // eslint-disable-line
    this[productsRequest] = this[Products].query(conditions, (products) => {
      products.forEach((item) => {
        item.sku = item.packs[0].sku.match(/^(\d{2}[a-z]{2,3}\d{2,3})/)[0] || '-';
      });
      this.list = products;
      this.count = products.length;
    });
  }
}

ProductsController.$inject = ['Products'];
