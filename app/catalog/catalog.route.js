function catalogConfig($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog', {
    url: '/catalog',
    abstract: true,
    template: '<ui-view/>',
    ncyBreadcrumb: { label: 'Каталог' },
  });
}

export default catalogConfig;
