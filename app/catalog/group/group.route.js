/**
 * Catalog groups routes config
 */

export default function groupRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.group', {
    url: '/category/:categoryId/group/:groupId',
    ncyBreadcrumb: {
      label: '{{ vm.title }}',
      parent: 'home.catalog.menu',
    },
    resolve: {
      group: (Groups, $stateParams, $log, $q) => { // eslint-disable-line
        'ngInject';

        const groupId = $stateParams.groupId;
        const categoryId = $stateParams.categoryId;

        if (groupId === 'new') { return new Groups({ categoryId }); }

        return Groups.get({ groupId, categoryId }).$promise.then((group) => {
          $log.debug('Group has been loaded', group);
          return group;
        });
      },
      categoryId: ($stateParams) => $stateParams.categoryId,
    },
    views: {
      '@': {
        controller: 'GroupController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/group/group.html',
      },
    },
  });
}
