/**
 * Catalog groups module
 */

import route from './group.route';
import controller from './group.controller';

export default angular
  .module('admin.catalog.group', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'toastr',
  ])
  .config(route)
  .controller('GroupController', controller)
  .name;
