/**
 * Catalog groups controller
 */

const $log = Symbol('$log');
const toastr = Symbol('toastr');
const $state = Symbol('$state');

class GroupController {
  constructor(group, categoryId, _$log_, _toastr_, _$state_) {
    this[$log] = _$log_;
    this[toastr] = _toastr_;
    this[$state] = _$state_;
    this.item = this.group = group;
    this.categoryId = categoryId;

    this.isNew = !this.item._id;

    if (this.isNew) {
      this.title = 'Новая группа';
    } else {
      this.title = this.item.name;
    }
  }

  /**
   * Save category
   */
  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      this[toastr].error('Проверьте корректность введенных данных');
    } else {
      if (this.isNew) {
        this.item.$save({ categoryId: this.categoryId }, () => {
          this[toastr].info('Группа добавлена');
          this[$state].go('home.catalog.menu');
        }, () => {
          this[toastr].error('Ошибка сервера');
        });
      } else {
        this.item.$update({ categoryId: this.categoryId })
          .then(() => {
            this[toastr].info('Группа сохранена');
            this[$state].go('home.catalog.menu');
          }, () => {
            this[toastr].error('Ошибка сервера');
          });
      }
    }
  }
}

GroupController.$inject = ['group', 'categoryId', '$log', 'toastr', '$state'];

export default GroupController;
