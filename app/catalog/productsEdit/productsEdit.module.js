import route from './productsEdit.route';
import controller from './productsEdit.controller';

export default angular
  .module('admin.catalog.products.edit', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'toastr',
  ])
  .config(route)
  .controller('ProductsEditController', controller)
  .name;
