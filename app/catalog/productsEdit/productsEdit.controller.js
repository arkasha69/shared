const _$log = new WeakMap();
const _Products = new WeakMap();
const $state = Symbol('$state');
const updateNotesVisible = Symbol();

export default class ProductsEditController {
  constructor(product, Products, Brands, ProductsGroups, lodash, toastr, _$state_, $log,
    addProductsModal) {
    this.Products = Products;
    this.toastr = toastr;
    this.addProductsModal = addProductsModal;

    _$log.set(this, $log);
    _Products.set(this, Products);

    // NOTE clean up
    this._toastr = toastr;
    this[$state] = _$state_;
    this._$log = $log;
    this._lodash = lodash;

    this.PACKS_LIMIT = 3;

    this.careIcon = ['Ежедневный уход', 'Интенсивный уход'];
    this.relevantProducts = ['Похожие товары', 'Комплексный уход'];

    this.item = this.product = product;

    this.isNew = !product._id;

    this.title = (product._id ? product.name : 'Новый товар');

    // Sortable options
    this.sortableOptions = {
      stop: () => {
        this.item.groups.forEach((group) => {
          group.products.forEach((productItem, index) => {
            productItem.order = index;
          });
        });
      },
    };

    if (this.isNew) {
      this.item.packs = [{}];
      this.item.meta = {};

      this.brands = Brands.query();
      this.productsGroups = ProductsGroups.query();
      this.item.descriptions = [{
        title: 'Воздействие',
        text: '',
      }, {
        title: 'Применение',
        text: '',
      }, {
        title: 'Активные компоненты',
        text: '',
      }, {
        title: 'Состав',
        text: '',
      }];
    } else {
      if (!angular.isArray(this.item.descriptions)) { this.item.descriptions = []; }
      if (this.item.icons.care && this.careIcon.indexOf(this.item.icons.care) === -1) {
        this.careIcon = [].concat(this.item.icons.care, this.careIcon);
      }
    }

    this[updateNotesVisible]();
  }

  refreshSelectItem($select, list) {
    if (!$select.search) {
      $select.items = list;
    } else {
      $select.items = [].concat($select.search, list);
    }
  }

  moveUp(list, index) {
    if (index === 0) { return; }

    const item = list[index];
    list[index] = list[index - 1];
    list[index - 1] = item;
  }

  moveDown(list, index) {
    if (index === list.length - 1) { return; }

    const item = list[index];
    list[index] = list[index + 1];
    list[index + 1] = item;
  }

  showNotes(item) {
    item.showNotes = true;
  }

  addPack() {
    const packs = this.item.packs;

    if (packs.length < this.PACKS_LIMIT) { packs.push({ new: true }); }
  }

  removePack(pack) {
    const packs = this.item.packs;
    for (let i = 0, len = packs.length; i < len; i++) {
      if (packs[i] === pack) {
        packs.splice(i, 1);
        break;
      }
    }
  }

  [updateNotesVisible]() {
    this.item.descriptions.forEach((item) => item.showNotes = !!item.notes);
  }

  /**
   * Adding one extra description item
   */
  addDescription() {
    this.item.descriptions.push({});
  }

  /**
   * Remove extra description item
   */
  removeDescription(item) {
    this._lodash.remove(this.item.descriptions, item);
  }

  /** Add new group of products */
  addGroup() {
    if (!angular.isArray(this.item.groups)) { this.item.groups = []; }

    this.item.groups.push({ title: '', products: [] });
  }

  /** Remove group of products */
  removeGroup(item) {
    this._lodash.remove(this.item.groups, item);
  }

  addProducts(group) {
    this.addProductsModal.open(group.products)
      .then((newProducts) => {
        newProducts.forEach((newProduct) => {
          if (!group.products.find((item) => item._id === newProduct._id)) {
            group.products.push(newProduct);
          }
        });
      });
  }

  /** Remove product from group */
  removeProduct(group, product) {
    this._lodash.remove(group.products, product);
  }

  /**
   * Save form changes
   */
  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      this._toastr.error('Проверьте корректность введенных данных', 'Товары', { timeOut: 3000 });
    } else {
      if (this.isNew) {
        this.item.$save((result) => {
          this.item = this.product = result;
          this.isNew = false;
          this.title = this.item.name;
          this._toastr.info('Добавлено', 'Товары');
          this[updateNotesVisible]();
          this[$state].go(this[$state].current, { id: result._id }, { notify: false });
        });
      } else {
        this.item.$update()
          .then(() => {
            this.title = this.item.name;
            this._toastr.info('Сохранено', 'Товары');
            this[updateNotesVisible]();
          });
      }
    }
  }
}

ProductsEditController.$inject = [
  'product', 'Products', 'Brands', 'ProductsGroups', 'lodash', 'toastr', '$state', '$log',
  'addProductsModal',
];
