export default function productsEditRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.productsEdit', {
    url: '/products/:id',
    ncyBreadcrumb: {
      label: '{{ vm.title }}',
      parent: 'home.catalog.products',
    },
    resolve: {
      product: (Products, $stateParams) => {
        const id = $stateParams.id;
        if (id === 'new') { return new Products(); }

        return Products.get({ id }).$promise;
      },
    },
    views: {
      '@': {
        controller: 'ProductsEditController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/productsEdit/productsEdit.html',
      },
    },
  });
}
