const Prices = Symbol('Prices');
const toastr = Symbol('toastr');

export default class PricesController {
  constructor(_Prices_, _toastr_) {
    this[Prices] = _Prices_;
    this[toastr] = _toastr_;
    this.inProcess = false;
  }

  update() {
    this.inProcess = true;

    this[Prices].query().$promise
      .then((result) => {
        this.items = result;
        this.inProcess = false;
        this[toastr].info('Цены обновлены');
      }, () => {
        this[toastr].error('Ошибка обновления');
      });
  }
}

PricesController.$inject = ['Prices', 'toastr'];
