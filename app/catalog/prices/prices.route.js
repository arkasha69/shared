export default function tagsRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.prices', {
    url: '/prices',
    ncyBreadcrumb: { label: 'Обновление цен' },
    views: {
      '@': {
        controller: 'PricesController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/prices/prices.html',
      },
    },
  });
}
