import route from './prices.route';
import Controller from './prices.controller';

export default angular
  .module('admin.catalog.prices', [
    'ui.router',
    'ncy-angular-breadcrumb',
  ])
  .config(route)
  .controller('PricesController', Controller)
  .name;
