import route from './tagsEdit.route';
import controller from './tagsEdit.controller';

export default angular
  .module('admin.catalog.tagsEdit', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'toastr',
  ])
  .config(route)
  .controller('TagsEditController', controller)
  .name;
