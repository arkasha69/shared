const Products = Symbol('Products');

const toastr = Symbol('toastr');
const _ = Symbol('lodash');
const $state = Symbol('$state');
const $log = Symbol('$log');

const addProductsModal = Symbol('addProductsModal');


export default class TagsEditController {
  constructor(tag, _Products_, _lodash_, _toastr_, _$state_, _$log_, _addProductsModal_) {
    this[Products] = _Products_;
    this[_] = _lodash_;
    this[toastr] = _toastr_;
    this[$state] = _$state_;
    this[$log] = _$log_;
    this[addProductsModal] = _addProductsModal_;

    this.item = this.tag = tag;

    this.isNew = !tag._id;

    this.title = (tag._id ? tag.name : 'Новый тэг');

    // Sortable options
    this.sortableOptions = {
      stop: () => {
        this.item.products.forEach((product, index) => {
          product.order = index;
        });
      },
    };

    if (this.isNew) {
      this.item.products = [];
      this.item.meta = {};
    } else {
      if (!angular.isArray(this.item.descriptions)) { this.item.descriptions = []; }
    }
  }

  /** Add product */
  addProducts() {
    this[addProductsModal].open(this.item.products)
      .then((newProducts) => {
        newProducts.forEach((newProduct) => {
          if (!this.item.products.find((item) => item._id === newProduct._id)) {
            this.item.products.push(newProduct);
          }
        });
      });
  }

  /** Remove product from group */
  removeProduct(product) {
    this[_].remove(this.item.products, product);
  }

  /**
   * Save form changes
   */
  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      this[toastr].error('Проверьте корректность введенных данных');
    } else {
      if (this.isNew) {
        this.item.$save((result) => {
          this.item = this.tag = result;
          this.isNew = false;
          this.title = this.item.name;
          this[toastr].info('Тэг добавлен');
        });
      } else {
        this.item.$update()
          .then(() => {
            this.title = this.item.name;
            this[toastr].info('Сохранено');
          });
      }
    }
  }
}

TagsEditController.$inject = [
  'tag', 'Products', 'lodash', 'toastr', '$state', '$log', 'addProductsModal',
];
