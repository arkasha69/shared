export default function tagsEditRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.tagsEdit', {
    url: '/tags/:id',
    ncyBreadcrumb: {
      label: '{{ vm.title }}',
      parent: 'home.catalog.tags',
    },
    resolve: {
      tag: (Tags, $stateParams, $log) => {
        const id = $stateParams.id;

        if (id === 'new') { return new Tags(); }

        return Tags.get({ id }).$promise.then((result) => {
          $log.debug('Tag has been loaded', result);
          return result;
        });
      },
    },
    views: {
      '@': {
        controller: 'TagsEditController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/tagsEdit/tagsEdit.html',
      },
    },
  });
}
