const toastr = Symbol('toastr');
const _ = Symbol('lodash');
const $log = Symbol('$log');

export default class PagesEditController {
  constructor(page, _lodash_, _toastr_, _$log_) {
    this[_] = _lodash_;
    this[toastr] = _toastr_;
    this[$log] = _$log_;

    this.item = page;

    this.isNew = !page._id;

    if (this.isNew) {
      page.meta = {};
    }

    this.title = (page._id ? page.h1 : 'Новый страница');
  }

  /**
   * Save form changes
   */
  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      this[toastr].error('Проверьте корректность введенных данных');
    } else {
      if (this.isNew) {
        this.item.$save((result) => {
          this.item = result;
          this.isNew = false;
          this.title = this.item.h1;
          this[toastr].info('Страница добавлена');
        });
      } else {
        this.item.$update()
          .then(() => {
            this.title = this.item.h1;
            this[toastr].info('Сохранено');
          });
      }
    }
  }
}

PagesEditController.$inject = ['page', 'lodash', 'toastr', '$log'];
