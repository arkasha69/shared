export default function pagesEditRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.pagesEdit', {
    url: '/pages/:id',
    ncyBreadcrumb: {
      label: '{{ vm.title }}',
      parent: 'home.catalog.pages',
    },
    resolve: {
      page: (Pages, $stateParams, $log) => {
        const id = $stateParams.id;
        if (id === 'new') { return new Pages(); }

        return Pages.get({ id }).$promise.then((result) => {
          $log.debug('Page has been loaded', result);
          return result;
        });
      },
    },
    views: {
      '@': {
        controller: 'PagesEditController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/pagesEdit/pagesEdit.html',
      },
    },
  });
}
