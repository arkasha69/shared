import route from './pagesEdit.route';
import controller from './pagesEdit.controller';

export default angular
  .module('admin.catalog.pagesEdit', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'toastr',
  ])
  .config(route)
  .controller('PagesEditController', controller)
  .name;
