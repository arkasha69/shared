export default function brandsRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.brandsList', {
    url: '/brands',
    ncyBreadcrumb: { label: 'Торговые марки' },
    views: {
      '@': {
        controller: 'BrandsListController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/brands/brands.html',
      },
    },
  });

  $stateProvider.state('home.catalog.brandsEdit', {
    url: '/brands/:id',
    resolve: {
      brand: (Brands, $stateParams) => {
        if ($stateParams.id === 'new') { return new Brands(); }
        return Brands.get({ id: $stateParams.id }).$promise;
      },
    },
    ncyBreadcrumb: {
      label: '{{ vm.title }}',
      parent: 'home.catalog.brandsList',
    },
    views: {
      '@': {
        controller: 'BrandsEditController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/brands/brandsEdit.html',
      },
    },
  });
}
