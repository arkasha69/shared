/**
 * New and edit brand page controller
 */

const _toastr = new WeakMap();
const _$state = new WeakMap();

class BrandController {
  constructor(brand, toastr, $state) {
    'ngInject';

    _toastr.set(this, toastr);
    _$state.set(this, $state);

    /** Brand object */
    this.brand = brand;

    /** Is it new brand */
    this.isNew = !brand._id;

    /** Page title */
    this.title = (brand._id ? brand.name : 'Новая торговая марка');
  }

  /**
   * Save brand
   */
  save() {
    const toastr = _toastr.get(this);
    const $state = _$state.get(this);

    this.form.$setSubmitted();

    if (this.form.$invalid) {
      toastr.error('Проверьте корректность введенных данных', 'Торговые марки', { timeOut: 3000 });
    } else {
      if (this.isNew) {
        this.brand.$save(() => {
          toastr.info('Добавлено', 'Торговые марки');
          $state.go('^.brandsList');
        });
      } else {
        this.brand.$update()
          .then(() => {
            this.title = this.brand.name;

            toastr.info('Сохранено', 'Торговые марки');
          });
      }
    }
  }
}

export default BrandController;
