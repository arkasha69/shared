import route from './brands.route';
import BrandsListController from './brands.controller';
import BrandsEditController from './brandsEdit.controller';

angular
  .module('admin.catalog.brands', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'toastr',
  ])
  .config(route)
  .controller('BrandsListController', BrandsListController)
  .controller('BrandsEditController', BrandsEditController);

export default angular.module('admin.catalog.brands').name;
