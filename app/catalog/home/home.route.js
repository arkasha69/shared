export default function homeRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.home', {
    url: '/home',
    ncyBreadcrumb: { label: 'Главная страница' },
    resolve: {
      home: (Home) => Home.get().$promise,
    },
    views: {
      '@': {
        controller: 'HomeController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/home/home.html',
      },
    },
  });
}
