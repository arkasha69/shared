/**
 * Home page controller
 */

const Tags = Symbol('Tags');
const toastr = Symbol('toastr');
const _ = Symbol('lodash');

export default class HomePageController {
  constructor(home, _Tags_, _toastr_, _lodash_) {
    this[Tags] = _Tags_;
    this[toastr] = _toastr_;
    this[_] = _lodash_;

    this.item = home;

    this.newTag = null;

    // Default values
    if (!angular.isArray(home.tags)) { home.tags = []; }

    this.sortableOptions = {
      stop: () => { this.item.tags.forEach((tag, index) => tag.order = index); },
    };
  }

  /** Adding tag */
  addTag() {
    if (this.item.tags.find((tagItem) => tagItem._id === this.newTag._id)) {
      this[toastr].info('Указанный тэг уже добавлен');
    } else {
      this.newTag.order = this.item.tags.length;
      this.item.tags.push(this.newTag);
    }

    this.newTag = null;
  }

  /** Removing tag */
  removeTag(tag) {
    this[_].remove(this.item.tags, tag);
  }

  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      return this[toastr].error('Проверьте корректность введенных данных');
    }

    if (!this.item.tags.length) { return this[toastr].error('Добавьте хотя бы 1 тэг'); }

    this.item.$update().then(() => { this[toastr].info('Сохранено'); });
  }

  /**
   * Search tags by name
   * @param  {String}  name Tag name
   * @return {Promise}
   */
  getTags(name) {
    const conditions = { q: name, fields: 'name,url' };

    if (this.getTagRequest) { this.getTagRequest.$cancelRequest(); }
    this.getTagRequest = this[Tags].query(conditions);

    return this.getTagRequest.$promise;
  }
}

HomePageController.$inject = ['home', 'Tags', 'toastr', 'lodash'];
