import route from './home.route';
import Controller from './home.controller';

export default angular
  .module('admin.catalog.home', [
    'ui.router',
    'ncy-angular-breadcrumb',
  ])
  .config(route)
  .controller('HomeController', Controller)
  .name;
