export default menuRoute;

function menuRoute($stateProvider) {
  'ngInject';

  $stateProvider.state('home.catalog.menu', {
    url: '/menu',
    ncyBreadcrumb: {
      label: 'Меню',
    },
    resolve: {
      menu: (Menu, $log) => {
        'ngInject';

        return Menu.query().$promise
          .then((menu) => {
            $log.debug('Menu has been loaded', menu);
            return menu;
          });
      },
    },
    views: {
      '@': {
        controller: 'MenuController',
        controllerAs: 'vm',
        templateUrl: 'app/catalog/menu/menu.html',
      },
    },
  });
}
