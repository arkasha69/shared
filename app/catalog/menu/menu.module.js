import route from './menu.route';
import controller from './menu.controller';

export default angular
  .module('admin.catalog.menu', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'toastr',
  ])
  .config(route)
  .controller('MenuController', controller)
  .name;
