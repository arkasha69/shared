const $log = Symbol('$log');
const $state = Symbol('$state');
const toastr = Symbol('toastr');
const menuState = Symbol('menuState');

class menuController {
  constructor(_menu_, _$log_, _$state_, _toastr_, _menuState_) {
    this[$log] = _$log_;
    this[$state] = _$state_;
    this[toastr] = _toastr_;
    this[menuState] = _menuState_;
    this.menu = _menu_;

    this[menuState].init(this.menu);

    // Sortable setup
    this.sortableOptions = {};
  }

  get activeItem() {
    return this[menuState].activeItem;
  }

  get activeGroup() {
    return this[menuState].activeGroup;
  }

  /**
   * Set active top level menu item
   * @param {Object | null} item New active item
   */
  setActiveItem(item) {
    this[menuState].activeItem = item;
  }

  /**
   * Set active menu group
   * @param {Object | null} item New active group
   */
  setActiveGroup(group) {
    this[menuState].activeGroup = group;
  }

  /**
   * Check if first level is active
   * @param  {Object}  item Menu item
   * @return {Boolean}
   */
  isActiveItem(item) {
    return this[menuState].isActiveItem(item);
  }

  /**
   * Check if group is active
   * @param  {Object}  item Checked group
   * @return {Boolean}      [description]
   */
  isActiveGroup(group) {
    return this[menuState].isActiveGroup(group);
  }

  /**
   * Save changes
   */
  save() {
    let count = this.menu.length;
    const activeGroupId = this.activeGroup && this.activeGroup._id;

    // Updating order properties
    this.menu.forEach((item, index) => {
      item.order = index;

      item.groups.forEach((group, groupIndex) => {
        group.order = groupIndex;

        group.categories.forEach((category, categoryIndex) => {
          category.order = categoryIndex;
        });
      });

      item.$updateOrder(() => {
        if (--count === 0) {
          this.setActiveGroup(this.activeItem.groups.find((group) => group._id === activeGroupId));
          this[toastr].info('Сохранено');
        }
      }, () => {
        this[toastr].error('Ошибка сохранения');
      });
    });
  }

  /**
   * Add new group
   * @param  {Obejct} parent Parent category
   */
  newGroup(parent) {
    this[$state].go('home.catalog.group', { categoryId: parent._id, groupId: 'new' });
  }

  /**
   * Add new category
   * @param  {Object|null} parent Parent group or null
   */
  newCategory(category, group) {
    if (category && group) {
      this[$state].go('home.catalog.catalog', {
        categoryId: category._id,
        groupId: group._id,
        id: 'new',
      });
    } else {
      this[$state].go('home.catalog.category', { id: 'new' });
    }
  }
}

menuController.$inject = ['menu', '$log', '$state', 'toastr', 'menuState'];

export default menuController;
