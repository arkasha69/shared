/**
 * Blog router
 */
export default function blogConfig($stateProvider) {
  'ngInject';

  $stateProvider.state('home.blog', {
    url: '/blog',
    abstract: true,
    template: '<ui-view/>',
    ncyBreadcrumb: { label: 'Блог' },
  });
}
