const query = Symbol('query');
const searchFilter = Symbol('searchFilter');

export default class BlogPostsController {
  constructor(_searchFilter_, posts) {
    this[searchFilter] = _searchFilter_;
    this[query] = '';
    this.list = this.filtredList = posts;
    this.totalCount = this.count = posts.length;
  }

  set query(value) {
    if (value) {
      this.filtredList = this[searchFilter](this.list, value);
    } else {
      this.filtredList = this.list;
    }
    this.count = this.filtredList.length;
    this[query] = value;
  }

  get query() {
    return this[query];
  }

  clearFilters() {
    this.query = '';
  }
}

BlogPostsController.$inject = ['searchFilter', 'posts'];
