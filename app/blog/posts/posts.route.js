export default function postRouter($stateProvider) {
  'ngInject';

  $stateProvider.state('home.blog.posts', {
    url: '/posts',
    ncyBreadcrumb: { label: 'Посты' },
    resolve: {
      posts: (Posts, $log) =>
        Posts.query().$promise
          .then((posts) => {
            $log.debug('All posts has been loaded', posts);
            return posts;
          }),
    },
    views: {
      '@': {
        controller: 'BlogPostsController',
        controllerAs: 'vm',
        templateUrl: 'app/blog/posts/posts.html',
      },
    },
  });
}
