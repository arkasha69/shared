import route from './posts.route';
import Controller from './posts.controller';

export default angular
  .module('admin.blog.posts', [
    'ui.router',
    'ncy-angular-breadcrumb',
  ])
  .config(route)
  .controller('BlogPostsController', Controller)
  .name;
