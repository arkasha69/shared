import route from './blog.route';

/* Child modules */
import postsModule from './posts/posts.module';
import postModule from './post/post.module';

export default angular
  .module('admin.blog', [
    'ui.router',
    'ncy-angular-breadcrumb',
    postsModule,
    postModule,
  ])
  .config(route)
  .name;
