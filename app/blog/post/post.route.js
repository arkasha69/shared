export default function postRouter($stateProvider) {
  'ngInject';

  $stateProvider.state('home.blog.post', {
    url: '/posts/:id',
    ncyBreadcrumb: {
      label: '{{ vm.title }}',
      parent: 'home.blog.posts',
    },
    resolve: {
      post: (Posts, $log, $stateParams) => {
        const id = $stateParams.id;

        if (id === 'new') { return new Posts(); }

        return Posts.get({ id }).$promise.then((post) => {
          $log.debug('Post has been loaded', post);
          return post;
        });
      },
    },
    views: {
      '@': {
        controller: 'BlogPostController',
        controllerAs: 'vm',
        templateUrl: 'app/blog/post/post.html',
      },
    },
  });
}
