import route from './post.route';
import Controller from './post.controller';

export default angular
  .module('admin.blog.post', [
    'ui.router',
    'ncy-angular-breadcrumb',
  ])
  .config(route)
  .controller('BlogPostController', Controller)
  .name;
