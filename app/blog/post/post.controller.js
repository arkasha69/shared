
const toastr = Symbol('toastr');
const _ = Symbol('lodash');
const $state = Symbol('$state');
const $log = Symbol('$log');

const addProductsModal = Symbol('addProductsModal');


export default class BlogPostController {
  constructor(post, _lodash_, _toastr_, _$state_, _$log_, _addProductsModal_) {
    this[_] = _lodash_;
    this[toastr] = _toastr_;
    this[$state] = _$state_;
    this[$log] = _$log_;
    this[addProductsModal] = _addProductsModal_;

    this.item = this.post = post;

    this.isNew = !post._id;

    this.title = (post._id ? post.name : 'Новый пост');

    // Sortable options
    this.sortableOptions = {
      stop: () => {
        this.item.products.forEach((product, index) => {
          product.order = index;
        });
        this.item.productsSidebar.forEach((product, index) => {
          product.order = index;
        });
      },
    };

    if (this.isNew) {
      this.item.title = 'Рекомендованные товары';
    }
    if (!angular.isArray(this.item.products)) { this.item.products = []; }
    if (!angular.isArray(this.item.productsSidebar)) { this.item.productsSidebar = []; }
  }

  /** Add product */
  addProducts(productsList) {
    this[addProductsModal]
      .open(productsList)
      .then((newProducts) => {
        for (const newProduct of newProducts) {
          if (!productsList.find((item) => item._id === newProduct._id)) {
            productsList.push(newProduct);
          }
        }
      });
  }

  /** Remove product from group */
  removeProduct(product, productsList) {
    this[_].remove(productsList, product);
  }

  /**
   * Save form changes
   */
  save() {
    this.form.$setSubmitted();

    if (this.form.$invalid) {
      this[toastr].error('Проверьте корректность введенных данных');
    } else {
      if (this.isNew) {
        this.item.$save((result) => {
          this.item = this.tag = result;
          this.isNew = false;
          this.title = this.item.name;
          this[toastr].info('Пост добавлен');
        }, (response) => {
          this[toastr].error(`Ошибка сервера "${response.data}"`);
        });
      } else {
        this.item.$update()
          .then(() => {
            this.title = this.item.name;
            this[toastr].info('Сохранено');
          }, (response) => {
            this[toastr].error(`Ошибка сервера "${response.data}"`);
          });
      }
    }
  }
}

BlogPostController.$inject = [
  'post', 'lodash', 'toastr', '$state', '$log', 'addProductsModal',
];
