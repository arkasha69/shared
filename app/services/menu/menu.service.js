/**
 * Menu service
 */
export default function MenuService($resource) {
  'ngInject';

  return $resource('/v2/menu/:id', { id: '@_id' }, {
    update: { method: 'PUT' },
    updateOrder: { method: 'PUT', params: { updateOrder: true } },
  });
}
