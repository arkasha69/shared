export default function ProductsService($resource) {
  'ngInject';

  return $resource('/v2/products/:id', { id: '@_id' }, {
    query: { method: 'GET', isArray: true, cancellable: true },
    get: { method: 'GET', cancellable: true },
    update: { method: 'PUT' },
  });
}
