export default function GroupService($resource) {
  'ngInject';

  return $resource('/v2/menu/:categoryId/groups/:groupId', { groupId: '@_id' }, {
    get: { method: 'GET', cancellable: true },
    update: { method: 'PUT' },
  });
}
