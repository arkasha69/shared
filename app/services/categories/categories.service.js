export default function CategoriesService($resource) {
  'ngInject';

  return $resource('/v2/menu/:id', { id: '@_id' }, {
    get: { method: 'GET', cancellable: true },
    update: { method: 'PUT' },
  });
}
