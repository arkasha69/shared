/**
 * Brands service
 */
export default function BrandService($resource) {
  'ngInject';

  return $resource('/v2/tags/:id', { id: '@_id' }, {
    query: { method: 'GET', isArray: true, cancellable: true },
    get: { method: 'GET', cancellable: true },
    update: { method: 'PUT' },
  });
}
