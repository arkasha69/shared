/**
 * Price update service
 */
export default function PricesService($resource) {
  'ngInject';

  return $resource('/v2/prices');
}
