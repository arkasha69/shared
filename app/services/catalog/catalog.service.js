export default function CatalogService($resource) {
  'ngInject';

  return $resource('/v2/categories/:id', { id: '@_id' }, {
    get: { method: 'GET', cancellable: true },
    update: { method: 'PUT' },
  });
}
