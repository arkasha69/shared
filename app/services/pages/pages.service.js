/**
 * Brands service
 */
export default function PagesService($resource) {
  'ngInject';

  return $resource('/v2/pages/:id', { id: '@_id' }, {
    get: { method: 'GET', cancellable: true },
    update: { method: 'PUT' },
  });
}
