/**
 * Home page service
 */
export default function HomePageService($resource) {
  'ngInject';

  return $resource('/v2/home-page/', {}, {
    update: { method: 'PUT' },
  });
}
