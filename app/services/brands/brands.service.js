/**
 * Brands service
 */
export default function BrandService($resource) {
  'ngInject';

  return $resource('/v2/brands/:id', { id: '@_id' }, {
    update: { method: 'PUT' },
  });
}
