/**
 * Menu state service
 */
const $localStorage = Symbol('$localStorage');
const activeItem = new WeakMap();
const activeGroup = new WeakMap();

export default class MenuStateService {
  constructor(_$localStorage_) {
    this[$localStorage] = _$localStorage_;

    activeItem.set(this, null);
    activeGroup.set(this, null);
  }

  /**
   * Init state
   */
  init(items = []) {
    let group;
    let item;

    if (items.length === 0) { return; }

    // Setting active item
    if (!this[$localStorage].activeMenuItemId) {
      item = items[0];
    } else {
      item = items.find((i) => i._id === this[$localStorage].activeMenuItemId);
    }

    if (!item) { return; }

    this[$localStorage].activeMenuItemId = item._id;
    activeItem.set(this, item);

    if (!item.groups.length) { return; }

    // Setting active group
    if (!this[$localStorage].activeMenuGroupId) {
      group = item.groups[0];
    } else {
      group = item.groups.find((i) => i._id === this[$localStorage].activeMenuGroupId);
    }

    if (!group) { return; }

    this[$localStorage].activeMenuGroupId = group._id;
    activeGroup.set(this, group);
  }

  /**
   * Check is item active
   */
  isActiveItem(item) {
    return activeItem.get(this) === item;
  }

  /**
   * Check is group active
   */
  isActiveGroup(group) {
    return activeGroup.get(this) === group;
  }

  /**
   * Set active item
   */
  set activeItem(item) {
    this[$localStorage].activeMenuItemId = item._id;
    activeItem.set(this, item);

    if (item.groups.length) {
      this.activeGroup = item.groups[0];
    }
  }

  /**
   * Get active item
   */
  get activeItem() {
    return activeItem.get(this);
  }

  /**
   * Set active group
   */
  set activeGroup(group) {
    if (group) {
      this[$localStorage].activeMenuGroupId = group._id;
      activeGroup.set(this, group);
    } else {
      this[$localStorage].activeMenuGroupId = null;
      activeGroup.set(this, null);
    }
  }

  /**
   * Get active item
   */
  get activeGroup() {
    return activeGroup.get(this);
  }
}

MenuStateService.$inject = ['$localStorage'];
