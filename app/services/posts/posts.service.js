/**
 * Posts service
 */
export default function PostsService($resource) {
  'ngInject';

  return $resource('/v2/posts/:id', { id: '@_id' }, {
    update: { method: 'PUT' },
  });
}
