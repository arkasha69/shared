/**
 * Products groups service
 */
export default function ProductsGroupsService($resource) {
  'ngInject';

  return $resource('/v2/products-groups/:id', { id: '@_id' }, {
    update: { method: 'PUT' },
  });
}
