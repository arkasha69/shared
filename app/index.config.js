function configApp($logProvider, $locationProvider, $httpProvider,
  $breadcrumbProvider, toastrConfig) {
  'ngInject';

  /* Enable log */
  /* TODO Make depand of enviroment */
  $logProvider.debugEnabled(true);

  /* HTML 5 mode */
  $locationProvider.html5Mode(true);

  /** Interceptor setup */
  $httpProvider.interceptors.push((config) => {
    'ngInject';

    return {
      request: function request(params) {
        if (params.url.indexOf('/v2/') === 0) { params.url = config.apiEndpoint + params.url; }
        return params;
      },
    };
  });

  /** Breadcrumb setup */
  $breadcrumbProvider.setOptions({
    includeAbstract: true,
    templateUrl: 'app/components/breadcrumb/breadcrumb.html',
  });

  /** Toastr config */
  angular.extend(toastrConfig, {
    preventOpenDuplicates: true,
    closeButton: true,
    closeHtml: '<button>&times;</button>',
    timeOut: 2000,
  });
}

export default configApp;
