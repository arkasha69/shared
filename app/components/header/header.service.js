const menu = Symbol();

export default class HeaderService {
  constructor() {
    'ngInject';

    /** Menu items */
    this[menu] = [{
      title: 'Магазин',
      icon: null,
      state: 'home.catalog',
      children: [{
        title: 'Каталог',
        icon: 'icon-menu',
        state: 'home.catalog.menu',
      }, {
        title: 'Товары',
        icon: 'icon-bag',
        state: 'home.catalog.products',
      }, {
        title: 'Торговые марки',
        icon: 'icon-chemistry',
        state: 'home.catalog.brandsList',
      }, {
        title: 'Группы товаров',
        icon: 'icon-list',
        state: 'home.catalog.productsGroups',
      }, {
        title: 'Тэги',
        icon: 'icon-tag',
        state: 'home.catalog.tags',
      }, {
        title: 'Страницы',
        icon: 'icon-book-open',
        state: 'home.catalog.pages',
      }, {
        title: 'Главная страница',
        icon: 'icon-home',
        state: 'home.catalog.home',
      }, {
        title: 'Обновление цен',
        icon: 'icon-wallet',
        state: 'home.catalog.prices',
      }],
    }, {
      title: 'Блог',
      icon: null,
      state: 'home.blog',
      children: [{
        title: 'Товары к постам',
        icon: 'icon-notebook',
        state: 'home.blog.posts',
      }],
    }];
  }

  /** Return menu items */
  get menu() {
    return this[menu];
  }
}
