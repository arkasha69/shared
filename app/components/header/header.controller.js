const headerService = Symbol('headerService');

export default class HeaderController {
  constructor(_headerService_) {
    this[headerService] = _headerService_;
  }

  get menu() {
    return this[headerService].menu;
  }
}

HeaderController.$inject = ['headerService'];
