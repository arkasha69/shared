const $uibModalInstance = Symbol('$uibModalInstance');
const Products = Symbol('Products');
const productsRequest = Symbol('productsRequest');
const query = Symbol('query');
const getProducts = Symbol('getProducts');
const selected = Symbol('selected');

export default class AddProductsController {
  constructor(_$uibModalInstance_, _Products_, products) {
    this[$uibModalInstance] = _$uibModalInstance_;
    this[Products] = _Products_;

    this[productsRequest] = null;
    this[selected] = new Set();

    products.forEach((product) => this[selected].add(product._id));

    /**
     * Selected products list
     * @type {Array}
     */
    this.selected = [];

    /**
     * List of product
     * @type {Array}
     */
    this.list = [];
  }

  /**
   * Search query setter
   */
  set query(value) {
    this[query] = value;

    if (value && value.length > 4) {
      this[getProducts](value).then((list) => this.list = list);
    }
  }

  /**
   * Search query getter
   */
  get query() {
    return this[query];
  }

  /**
   * Get list of products
   * @private
   */
  [getProducts](name) {
    const conditions = { q: name, fields: 'name,packs,texts,url,img' };

    this[productsRequest] && this[productsRequest].$cancelRequest(); // eslint-disable-line
    this[productsRequest] = this[Products].query(conditions);

    return this[productsRequest].$promise;
  }

  /**
   * Adding new products
   * @param {Product} product New product item
   */
  add(product) {
    if (this[selected].has(product._id)) { return; }

    this.selected.push(product);
    this[selected].add(product._id);
    this.query = '';
  }

  /**
   * Removing products from selected new products
   */
  remove(product) {
    this[selected].delete(product._id);
    this.selected = this.selected.filter((item) => item._id !== product._id);
  }

  /**
   * Check is product already selected
   */
  isSelected() {
    const selectedSet = this[selected];

    return function isSelectedFn(value) {
      return !selectedSet.has(value._id);
    };
  }

  /**
   * CLose modal and pass selected list
   */
  save() {
    this[$uibModalInstance].close(this.selected);
  }

  /**
   * Close modal with out data
   */
  cancel() {
    this[$uibModalInstance].dismiss();
  }
}

AddProductsController.$inject = ['$uibModalInstance', 'Products', 'products'];
