/**
 * Brands service
 */
export default function addProductsService($uibModal) {
  'ngInject';

  function open(productsList = []) {
    return $uibModal.open({
      controller: 'addProductsController',
      controllerAs: 'vm',
      templateUrl: 'app/components/addProducts/addProducts.html',
      size: 'lg',
      animation: false,
      resolve: {
        products: () => productsList,
      },
      windowClass: 'modal_products',
    }).result;
  }

  return {
    open,
  };
}
