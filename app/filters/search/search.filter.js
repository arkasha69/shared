export default function searchFilterFactory() {
  return function searchFilter(list, query) {
    const pattern = RegExp(query.split(' ').join('.*'), 'i');

    return list.filter((item) => pattern.test(item.name));
  };
}
