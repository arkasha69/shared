function homeRoute($stateProvider, $urlRouterProvider) {
  'ngInject';

  $stateProvider.state('home', {
    url: '',
    abstract: true,
    template: '<ui-view/>',
    ncyBreadcrumb: { label: 'Главная' },
  });

  $urlRouterProvider.otherwise('/catalog/brands');
}

export default homeRoute;
